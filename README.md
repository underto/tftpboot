# _TO*hacklab netboot

Istruzioni base:

* clonare il repository in una cartella (es.: /tftpboot)

Per completare il lavoro:

* nella cartella, creare una cartella 'images'
* scaricare le ISO e i pacchetti di netboot grml da http://download.grml.org (tutti della stessa release!)
  * grml96-full*.iso
  * grml_netboot_package_grml32*.tar.bz2
  * grml_netboot_package_grml64*.tar.bz2
* montare l'immagine iso e copiare il contenuto in `images/grml96`
* estrarre i due netboot package rispettivamente in `images/grml32_boot` e `images/grml64
_boot`
* scaricare la iso di SliTaz da http://mirror.slitaz.org, montarla e copiare il contenuto
 in `images/slitaz/boot` 
* scaricare in `images` gli iPXE precompilatii (*.lkrn) da http://boot.salstar.sk e https://netboot.xyz (da rinominare rispettivamente: `salstar.lkrn` e `netboot.xyz.lkrn`

